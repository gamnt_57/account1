﻿#encoding UTF-8

require 'spec_helper'

 feature "xóa phiếu chi" do
  let!(:user) { FactoryGirl.create(:user)}
  let!(:chit) { FactoryGirl.create(:chit, serial: "1", user: user)}
  
  before do 
    sign_in_as!(user)
    visit chits_path
	  click_link "1"
  end
  
  scenario "xóa 1 phiếu chi" do
	click_link "Xóa phiếu chi"
	expect(page).to have_content("phiếu chi đã được xóa")
	visit"/"
	expect(page).to have_no_content("hovaten")
  end
 end
 