﻿#encoding UTF-8

require 'spec_helper'

 feature "xóa phiếu thu" do
  let!(:user) { FactoryGirl.create(:user)}
  let!(:bill){ FactoryGirl.create(:bill, name: "hovaten", user: user) }
  before do 
    sign_in_as!(user)
    visit bills_path
	  click_link "hovaten"
  end
  scenario "xóa 1 phiếu thu" do
   click_link "Xóa phiếu thu"
   expect(page).to have_content("phiếu thu đã được xóa")
	visit bills_path
	expect(page).to have_no_content("hovaten")
  end
 end