﻿#encoding UTF-8

require 'spec_helper'

feature "Sửa phiếu chi" do
 let!(:user) { FactoryGirl.create(:user)}
 let!(:chit) do
   chit = FactoryGirl.create(:chit, serial: "1")
   chit.update(user: user)
   chit
 end

 before do 
   sign_in_as!(user)
   visit chits_path
   click_link "1"
   click_link "Sửa phiếu chi"
 end
 scenario "Cập nhật phiếu chi" do
   
  fill_in 'Số phiếu',with:'1'
  fill_in 'Tên người tạo phiếu',with:'hovaten'
  fill_in 'Địa chỉ',with:'noitaophieu'
  fill_in 'Lý do chi',with:'nophocphi'
  fill_in 'Số tiền',with:'200000'
  fill_in 'Nguồn kinh phí',with:'doan'
  fill_in 'Chủ nhiệm khoa',with:'hoten'
  fill_in 'Người nhận tiền',with:'hoten'
  fill_in 'Thủ quỹ',with:'hoten'
  click_button "Cập nhật phiếu chi"
   
   expect(page).to have_content("phiếu chi đã cập nhật xong")
  end
end