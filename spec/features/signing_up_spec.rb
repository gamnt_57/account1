﻿#encoding UTF-8
require 'spec_helper'


feature 'Đăng ký' do
# let!(:user) { FactoryGirl.create(:user)}
# before do
#  sign_in_as!(user)
# end 
 
 scenario 'Đăng ký thành công' do
  visit '/'
  click_link 'Đăng ký'
  fill_in "email", with: "user@example.com"
  fill_in "password", with: "nguyenthigam"
  fill_in "password confirmation", with: "nguyenthigam"
  click_button "Đăng ký"
  expect(page).to have_content("bạn đã đăng ký thành công")
 end
end 
