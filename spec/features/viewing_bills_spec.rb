﻿#encoding UTF -8

require 'spec_helper'

feature "xem phiếu thu" do
 scenario "danh sách phiếu thu" do
  user = FactoryGirl.create(:user)
  bill = FactoryGirl.create(:bill, name: "hovaten")
  bill.update(user: user)
  visit bills_path
  click_link 'hovaten'
  expect(page.current_url).to eql(bill_url(bill))
 end
end