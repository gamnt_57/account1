﻿#encoding UTF-8
require 'spec_helper'

feature "đăng nhập" do
 
 scenario 'đăng nhập theo form' do
  user = FactoryGirl.create(:user, :name=>"gamnt")
  visit '/'
  click_link 'Đăng nhập'
  fill_in 'Name', with: user.name
  fill_in 'Password', with: user.password
  click_button "Đăng nhập"
  expect(page).to have_content("đăng nhập thành công")
 end
end