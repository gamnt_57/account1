﻿#encoding UTF-8
require 'spec_helper'

feature "tạo phiếu thu" do
  let!(:user) { FactoryGirl.create(:user)}
  
  before do 
    sign_in_as!(user)
    visit '/'
    click_link 'Phiếu thu mới'
   # message = "bạn cần đăng nhập để tiếp tục"
   # expect(page).to have_content(message)
   # fill_in "Name" , with: user.name
   # fill_in "Password" , with: user.password
   # click_button "Đăng nhập"
   # click_link 'Phiếu thu mới'
  end

  scenario "có thể tạo 1 phiếu thu" do
    fill_in 'Số phiếu',with:'1'
    fill_in 'Tên người tạo phiếu',with:'hovaten'
    fill_in 'Địa chỉ',with:'noitaophieu'
    fill_in 'Lý do thu',with:'nophocphi'
    fill_in 'Số tiền',with:'200000'
    fill_in 'Nguồn kinh phí',with:'doan'
    fill_in 'Chủ nhiệm khoa',with:'hoten'
    fill_in 'Người nộp tiền',with:'hoten'
    fill_in 'Thủ quỹ',with:'hoten'
    click_button'Tạo phiếu'
    expect(page).to have_content('phiếu thu đã tạo')
  end
end