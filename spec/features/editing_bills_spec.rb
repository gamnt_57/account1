﻿#encoding UTF-8

require 'spec_helper'

feature "Sửa phiếu thu" do
 let!(:user) { FactoryGirl.create(:user)}
 let!(:bill) do
   bill = FactoryGirl.create(:bill, serial: "1")
   bill.update(user: user)
   bill
 end
 
  before do 
      sign_in_as!(user)
      visit bills_path
      click_link "hovaten"
      click_link "Sửa phiếu thu"
  end
  scenario "Cập nhật phiếu thu" do
  fill_in 'Số phiếu',with:'1'
  fill_in 'Tên người tạo phiếu',with:'hovaten'
  fill_in 'Địa chỉ',with:'noitaophieu'
  fill_in 'Lý do thu',with:'nophocphi'
  fill_in 'Số tiền',with:'200000'
  fill_in 'Nguồn kinh phí',with:'doan'
  fill_in 'Chủ nhiệm khoa',with:'hoten'
  fill_in 'Người nộp tiền',with:'hoten'
  fill_in 'Thủ quỹ',with:'hoten'
  click_button "Cập nhật phiếu thu"
   
   expect(page).to have_content("phiếu thu đã cập nhật xong")
  end
end