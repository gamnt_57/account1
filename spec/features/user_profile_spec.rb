﻿#encoding UTF-8
require 'spec_helper'

feature "trang thông tin cá nhân" do
 scenario "kiểm tra" do
  user = FactoryGirl.create(:user, name:"gamnt", email: "gam@gmail.com")
  visit users_path
  click_link 'gamnt'
  
  expect(page.current_url).to eql(user_url(user))
 end
end

feature "sửa thông tin" do
 scenario "cập nhật thông tin người dùng" do
  user = FactoryGirl.create(:user, name:"gamnt")
  visit users_path
  click_link "gamnt"
  click_link "sửa thông tin"
  fill_in "Username", with: "username"
  click_button "cập nhật thông tin"
  expect(page).to have_content("thông tin đã được cập nhật")
 end
 end
 feature "xóa người dùng" do
 scenario "xóa 1 người dùng" do
  user = FactoryGirl.create(:user, name:"gamnt")
  visit users_path
  click_link "gamnt"
  click_link "xóa người dùng"
  
  expect(page).to have_content("người dùng đã được xóa")
  
  visit users_path
  expect(page).to have_no_content("gamnt")
 end
end