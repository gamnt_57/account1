﻿#encoding UTF-8
require 'spec_helper'
feature "thanh toan" do
let!(:user) { FactoryGirl.create(:user)}
let!(:chit) { FactoryGirl.create(:chit, serial: "1", user: user)}

  before do
    sign_in_as!(user)
    visit chits_path
    click_link "1"
    click_link "Thanh toán"
  end

 scenario "thanh toan qua phieu chi" do
  fill_in 'Số phiếu',with:'1'
  fill_in 'Tên người tạo phiếu',with:'hovaten'
  fill_in 'Địa chỉ',with:'noitaophieu'
  fill_in 'Lý do thu',with:'nophocphi'
  fill_in 'Số tiền',with:'200000'
  fill_in 'Nguồn kinh phí',with:'doan'
  fill_in 'Chủ nhiệm khoa',with:'hoten'
  fill_in 'Người nộp tiền',with:'hoten'
  fill_in 'Thủ quỹ',with:'hoten'
  click_button'Tạo phiếu'
  expect(page).to have_content('phiếu thu đã tạo')
 end
end