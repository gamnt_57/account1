#encoding:UTF-8
module AuthenticationHelpers
  def sign_in_as!(user)
    visit '/signin'
    fill_in "Name" , with: user.name
    fill_in "Password" , with: user.password
    click_button "Đăng nhập"
    expect(page).to have_content("đăng nhập thành công")
  end
end

RSpec.configure do|c|
  c.include AuthenticationHelpers, type: :feature
end