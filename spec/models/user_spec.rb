﻿#encoding: UTF-8
require 'spec_helper'

describe User do
  describe "mật khẩu" do
    it "cần 1 mật khẩu và xác nhận nó để lưu" do
      u = User.new(name: "gamnt")
      u.save
      expect(u).to_not be_valid
      u.password = "password"
      u.password_confirmation = ""
      u.save
      expect(u).to_not be_valid
      u.password_confirmation = "password"
      u.save
      expect(u).to be_valid
    end

    it "cần mật khẩu và xác nhận cho 1 người" do
      u = User.create(
          name: "gamnt",
          password: "nguyenthigam2",
          password_confirmation: "nguyenthigam")
      expect(u).to_not be_valid
    end
  end

  describe "xác nhận" do
    let(:user) { User.create(
        name: "gamnt",
        password: "nguyenthigam1",
        password_confirmation: "nguyenthigam1") }

    it "xác nhận đúng cho mật khẩu đúng" do
      expect(user.authenticate("nguyenthigam1")).to be
    end

    it "xác nhận sai cho mật khẩu sai" do
      expect(user.authenticate("nguyenthigam2")).to_not be
    end
  end
end
