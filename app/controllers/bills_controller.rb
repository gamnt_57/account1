﻿#encoding UTF-8
class BillsController < ApplicationController
 before_action :require_signin!, except: [:show, :index]
 def index
   @bills=Bill.all
 end
 
  def new
   @bill=Bill.new
   x=params[:sophieu]
   if(x==nil)
   else
   flash[:notice]="thanh toán cho phiếu chi số "+x
   end
  end
  
  def create
   @bill= Bill.new(bill_params)
   @bill.user = current_user
   if @bill.save
    flash[:notice]= "phiếu thu đã tạo"
	redirect_to @bill
	else
	#nothing
	end
  end
  
  def show
   @bill=Bill.find(params[:id])
  end
  
  def edit
   @bill=Bill.find(params[:id])
  end
  
  def update
   @bill= Bill.find(params[:id])
   @bill.update(bill_params)
   flash[:notice]= "phiếu thu đã cập nhật xong"
   redirect_to @bill
  end
  
  def destroy 
   @bill= Bill.find(params[:id])
   @bill.destroy
   flash[:notice]="phiếu thu đã được xóa"
   redirect_to bills_path
  end
  private
  def bill_params
   params.require(:bill).permit(:serial, :name, :address, :reason, :payment, :ability, :dean, :tabolator, :payer, :cashier)
  end
end
