﻿#encoding UTF-8
class UsersController < ApplicationController
 def index
  @users =User.all
 end
 def new
  @user = User.new
 end
 
 def create
  @user = User.new(user_params)
  if @user.save
   flash[:notice] = "bạn đã đăng ký thành công"
   redirect_to users_path
   else
   render :new
 end
end

 def show
    @user = User.find(params[:id])
 end

 def edit
    @user = User.find(params[:id])
 end
  
  def update
   @user=User.find(params[:id])
   @user.update(user_params)
   flash[:notice]="thông tin đã được cập nhật"
   redirect_to @user
  end
 
def destroy
  @user = User.find(params[:id])
  @user.destroy
  flash[:notice]= "người dùng đã được xóa"
  redirect_to users_path
end 
private
 def user_params
 params.require(:user).permit(:name, :password, :password_confirmation)
 end
end