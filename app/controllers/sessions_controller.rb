﻿#encoding UTF-8
class SessionsController < ApplicationController

 def new
   @user=User.new
 end
 
 def create
   user = User.where(:name => params[:signin][:name]).first
   if user && user.authenticate(params[:signin][:password])
     session[:user_id] = user.id
	   flash[:notice]="đăng nhập thành công"
	   redirect_to root_url 
   else
	   flash[:error]="đăng nhập không thành công"
	   render :new
   end
 end
end