# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

<<<<<<< HEAD
ActiveRecord::Schema.define(version: 20140522081945) do
=======
<<<<<<< HEAD
ActiveRecord::Schema.define(version: 20140522081945) do
=======
ActiveRecord::Schema.define(version: 20140518160432) do
>>>>>>> 488961c8cea0bca477082f42111113a59e3826a3
>>>>>>> 510c9705ac68f0c11233f36dc70459f62ad77396

  create_table "bills", force: true do |t|
    t.integer  "serial"
    t.string   "name"
    t.string   "address"
    t.string   "reason"
    t.integer  "payment"
    t.string   "ability"
    t.string   "dean"
    t.string   "tabolator"
    t.string   "payer"
    t.string   "cashier"
    t.integer  "chit_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "user_id"
  end

  add_index "bills", ["chit_id"], name: "index_bills_on_chit_id"
  add_index "bills", ["user_id"], name: "index_bills_on_user_id"

  create_table "chits", force: true do |t|
    t.integer  "serial"
    t.string   "name"
    t.string   "address"
    t.string   "reason"
    t.integer  "payment"
    t.string   "ability"
    t.string   "dean"
    t.string   "tabolator"
    t.string   "payer"
    t.string   "cashier"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "user_id"
  end

  add_index "chits", ["user_id"], name: "index_chits_on_user_id"

  create_table "users", force: true do |t|
    t.string   "name"
    t.string   "email"
    t.string   "password_digest"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "admin",           default: false
  end

end
