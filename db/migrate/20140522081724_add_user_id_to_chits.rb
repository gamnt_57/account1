class AddUserIdToChits < ActiveRecord::Migration
  def change
    add_reference :chits, :user, index: true
  end
end
