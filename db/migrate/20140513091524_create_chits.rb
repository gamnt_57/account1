class CreateChits < ActiveRecord::Migration
  def change
    create_table :chits do |t|
      t.integer :serial
      t.string :name
      t.string :address
      t.string :reason
      t.integer :payment
      t.string :ability
      t.string :dean
      t.string :tabolator
      t.string :payer
      t.string :cashier

      t.timestamps
    end
  end
end
